
import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Milan Dojchinovski
 * <milan (at) dojchinovski (dot) mk>
 * Twitter: @m1ci
 * www: http://dojchinovski.mk
 */
public class GateClient {
    
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    public void run(HttpServletResponse response) throws IOException{
        
        if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }        

        try {                
            
            File file = new File(Gate.getPluginsHome(), "Twitter");
            try {
                Gate.getCreoleRegister().registerDirectories(file.toURL());
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");
            ProcessingResource twitterTokenizer = (ProcessingResource) Factory.createResource("gate.twitter.tokenizer.TokenizerEN");
            ProcessingResource hashtagTokenizer = (ProcessingResource) Factory.createResource("gate.twitter.HashtagTokenizer");
            ProcessingResource tweetNormaliser = (ProcessingResource) Factory.createResource("gate.twitter.Normaliser");
            ProcessingResource sentenceSplitter = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            ProcessingResource twitterPOSTagger = (ProcessingResource) Factory.createResource("gate.twitter.pos.POSTaggerEN");
                        
            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
           
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(twitterTokenizer);
            annotationPipeline.add(hashtagTokenizer);
            annotationPipeline.add(tweetNormaliser);
            annotationPipeline.add(sentenceSplitter);
            annotationPipeline.add(twitterPOSTagger);
            
            String csvFileTweets = "/Users/Jan/Desktop/tweets_30.csv";
            BufferedReader tweets = null;
            
            String csvFileSentiments = "/Users/Jan/Desktop/sentiments.csv";
            BufferedReader sentiments = null;

           
            // create a document
            //Document document = Factory.newDocument("Come to the dark side “@gretcheneclark: Hey @apple, if you send me a free iPhone, I will publicly and ceremoniously burn my #BlackBerry.”");

            // TODO: Load all tweets from CSV into documents
            
            // TODO: Load whole emotion dictionary into array
            
            // create a corpus and add the document
            Corpus corpus = Factory.newCorpus("");

            try {
                
                tweets = new BufferedReader(new FileReader(csvFileTweets));
                 
                String line = "";
                String cvsSplitBy = ";";
                while ((line = tweets.readLine()) != null) {

                    String[] column = line.split(cvsSplitBy);

                    for (int i = 0; i < column.length; i++) {

                        if(i == 4) corpus.add(Factory.newDocument(column[i]));
                        //if( i == 1) System.out.println(column[i]);

                    }
                }

            } catch (FileNotFoundException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
            }
            
            //String[][] sentimentsCollection;
            List<Map<String, String>> sentimentsCollection = new ArrayList<>();
            
            try {
                
                sentiments = new BufferedReader(new FileReader(csvFileSentiments));
                
                String line = "";
                String cvsSplitBy = ";";
                
                int i = 0;
                
                while ((line = sentiments.readLine()) != null) {
                  
                    String[] column = line.split(cvsSplitBy);
                    
                    Map<String, String> map = new HashMap<String, String>();
                    
                    for (int j = 0; j < column.length; j++) {

                        if(j == 2) map.put("positive", column[j]);
                        if(j == 3) map.put("negative", column[j]);
                        if(j == 4) map.put("words", column[j]);
                        
                    }
                    
                    sentimentsCollection.add(i, map);
                    
                    i++;
                }

            } catch (FileNotFoundException e) {
                    e.printStackTrace();
            } catch (IOException e) {
                    e.printStackTrace();
            }

            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // loop through the documents in the corpus
            for(int i=0; i< corpus.size(); i++){
                
                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();

                FeatureMap futureMap = null;
                // get all Token annotations
                AnnotationSet annSetTokens = as_default.get("Token", futureMap);
                
                response.getWriter().println(doc.getContent() + "<br>");
                System.out.println("Number of Token annotations: " + annSetTokens.size());
                System.out.println(doc.getContent());

                ArrayList tokenAnnotations = new ArrayList(annSetTokens);

                //System.out.println("Token annotaions: " + tokenAnnotations.toString());
                
                float score = 0;
                
                // looop through the Token annotations
                for(int j = 0; j < tokenAnnotations.size(); ++j) {
                    
                    

                    // get a token annotation
                    Annotation token = (Annotation)tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    //Node isaStart = token.getStartNode();
                    //Node isaEnd = token.getEndNode();
                    //String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    //System.out.println("Token: " + underlyingString);
                    
                    // get the features of the token
                    FeatureMap tokenFeatures = token.getFeatures();
                    
                    if(tokenFeatures.get("kind").equals("word") && (
                        tokenFeatures.get("category").equals("VB")
                        || tokenFeatures.get("category").equals("VBD")
                        || tokenFeatures.get("category").equals("VBG")
                        || tokenFeatures.get("category").equals("JJ")
                        || tokenFeatures.get("category").equals("JJR")
                        || tokenFeatures.get("category").equals("JJS")
                        || tokenFeatures.get("category").equals("UH")
                        || tokenFeatures.get("category").equals("NN")
                        || tokenFeatures.get("category").equals("NNS")
                        || tokenFeatures.get("category").equals("NNP")
                        || tokenFeatures.get("category").equals("NNPS"))) {
                        
                        //sentimentsCollection
                        
                        for(int m = 0; m < sentimentsCollection.size(); m++) {
                        
                            String words = sentimentsCollection.get(m).get("words");
                            
                            //System.out.println(sentimentsCollection.get(m).toString());
                            //System.out.println(words);
                            
                            String[] splitted = words.split(" ");
                            
                            for(int n = 0; n < splitted.length; n++) {
                        
                                String[] parts = splitted[n].split("#");
                                String word = parts[0];
                                
                                if(tokenFeatures.get("string").equals(word)) {
                                    
                                    float wordScore = 0;
                                    wordScore += parseFloat(sentimentsCollection.get(m).get("positive"));
                                    wordScore -= parseFloat(sentimentsCollection.get(m).get("negative"));
                                    
                                    if(tokenFeatures.get("category").equals("VB")
                                        || tokenFeatures.get("category").equals("VBD")
                                        || tokenFeatures.get("category").equals("VBG")
                                        || tokenFeatures.get("category").equals("JJ")
                                        || tokenFeatures.get("category").equals("JJR")
                                        || tokenFeatures.get("category").equals("JJS")) {
                                    
                                        wordScore *= 1.5;
                                    
                                    }
                                    
                                    if(tokenFeatures.get("category").equals("UH")) {
                                    
                                        wordScore *= 3;
                                    
                                    }
                                    
                                    score += wordScore;
                                    //System.out.println("Token found!");
                                
                                }

                            }
                           
                        }
                                                                  
                        //System.out.println("Token string: " + tokenFeatures.get("string"));
                        //System.out.println("Token score: " + score);
                        //System.out.println("=====================================");
                        
                    }                    
                }
                
                response.getWriter().println("-------------------------------------<br>");
                System.out.println("-------------------------------------");
                
                if(score > 0.5) response.getWriter().println("<span style='color: green'>");
                else if(score < -0.5) response.getWriter().println("<span style='color: red'>");
                else response.getWriter().println("<span style='color: blue'>");
                
                response.getWriter().println("Total score: " + score + "<br>");
                System.out.println("Total score: " + score);
                
                response.getWriter().println("</span>");
                
                response.getWriter().println("=====================================<br>");
                System.out.println("=====================================");
                
                

            }
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("C:/Program Files/GATE_Developer_8.1");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("C:/Program Files/GATE_Developer_8.1/plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("C:/Program Files/GATE_Developer_8.1", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}